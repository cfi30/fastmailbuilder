FastMailBuilder
===============

A simple tool to send customized multipart emails with attachments in php.

Basic Usage
-----

```
<?php
require 'FastMailBuilder.php';

(new FastMailBuilder)
    ->to('John Doe', 'jdoe@domain.com')
    ->from('New Site', 'no-reply@yourdomain.com')
    ->subject('Order Confirmation')
    ->text(
        "Hello John Doe,

         We have successfully received your order of 42,000 french fries.
         They will be delivered to your office as soon as possible."
    )->html(
        "Hello <strong>John Doe</strong>,

         We have successfully received your order of <u>42,000 french fries</u>.
         They will be delivered to your office as soon as possible."
    )->send();
```
